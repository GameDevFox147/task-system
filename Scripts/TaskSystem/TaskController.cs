using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using UnityEngine;
using static Game.Task;

namespace Game
{
    public class TaskController : MonoBehaviour
    {
        public static TaskController Instance;

        private Canvas canvas;
        private Animation taskPanelAnim;

        [Space]
        [SerializeField] private RectTransform content;

        [SerializeField] private GameObject comingSoon;

        [Space]
        [SerializeField] private TextAsset json;

        [Space]
        [SerializeField] private Animation taskBtn;

        [Space]
        [SerializeField] private List<TaskObject> taskObjects;


        #region MonoBehaviour

        private void OnEnable()
        {
            TaskObject.TaskComplete = CheckAvailabilityTasks;
            TaskObject.TaskWaitingReward = CheckWaitingReward;

            CheckAvailabilityTasks();
            CheckWaitingReward();
        }

        private void OnDisable()
        {
            TaskObject.TaskComplete = null;
            TaskObject.TaskWaitingReward = null;
        }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }

            canvas = GetComponent<Canvas>();
            taskPanelAnim = GetComponent<Animation>();

            CreateTaskObjects();
        }

        private void CreateTaskObjects()
        {
            TaskInfo taskInfo = JsonConvert.DeserializeObject<TaskInfo>(json.text);

            taskObjects[0].SetData(taskInfo.Likes, TaskType.Likes);
            taskObjects[1].SetData(taskInfo.Followers, TaskType.Followers);
            taskObjects[2].SetData(taskInfo.Locations, TaskType.Locations);
            taskObjects[3].SetData(taskInfo.FortuneWheel, TaskType.FortuneWheel);
            taskObjects[4].SetData(taskInfo.VideoRecord, TaskType.VideoRecords);
        }

        #endregion

        private void CheckAvailabilityTasks()
        {
            if (taskObjects.Find(x => x.gameObject.activeSelf) == null)
            {
                comingSoon.SetActive(true);
            }
        }

        private void CheckWaitingReward()
        {
            taskObjects.ForEach(x =>
            {
                x.transform.SetAsLastSibling();
            });

            if (taskObjects.Find(x => x.WaitingReward) != null)
            {
                SetAnimationState(true);

                taskObjects.ForEach(x =>
                {
                    if (x.WaitingReward)
                    {
                        x.transform.SetAsFirstSibling();
                    }
                });
            }
            else
            {
                SetAnimationState(false);
            }
        }

        #region Animation

        public void SetAnimationState(bool state)
        {
            taskBtn.Stop();

            if (state) { taskBtn.Play("TaskButtonFlashing"); }
            else { taskBtn.Play("TaskButtonIdle"); }
        }

        public void TaskPanelSetActive(bool state)
        {
            if (state)
            {
                taskPanelAnim.Play("SmoothAppearance");

                canvas.enabled = state;

                content.anchoredPosition = Vector2.zero;
            }
            else
            {
                taskPanelAnim.Play("SmoothDisappearance");

                TimeUtility.Delay(taskPanelAnim.clip.length, () =>
                {
                    canvas.enabled = state;
                });
            }
        }

        #endregion
    }
}
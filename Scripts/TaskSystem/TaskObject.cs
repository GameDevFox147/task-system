using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static Game.Task;

namespace Game
{
    public class TaskObject : MonoBehaviour
    {
        public static Action TaskComplete;
        public static Action TaskWaitingReward;

        private string PP_KEY;

        [Header("UI")]
        private Color defaultColor = new Color(1f, 0.53f, 0f, 1f);
        private Color rewardColor = Color.white;

        [SerializeField] private Image icon;
        [SerializeField] private Image statusLine;

        [Space]
        [SerializeField] private TextMeshProUGUI info;
        [SerializeField] private TextMeshProUGUI title;
        [SerializeField] private TextMeshProUGUI target;
        [SerializeField] private TextMeshProUGUI reward;

        [Space]
        [SerializeField] private GameObject collectButton;

        [Header("Fields")]
        public TaskType taskType = TaskType.None;

        [Space]
        [SerializeField] private List<TaskData> taskData;
        [SerializeField] private List<TaskState> taskState;

        [Space]
        [SerializeField] private TaskData task;
        [SerializeField] private TaskState state;

        [Space]
        [SerializeField] private bool waitingReward = false;
        public bool WaitingReward => waitingReward;

        private void OnDestroy()
        {
            UnsubscribeOnEvent();
        }

        public void SetData(List<TaskData> taskData, TaskType taskType)
        {
            this.taskData = taskData;
            this.taskType = taskType;

            PP_KEY = "TaskType_" + taskType.ToString();

            LoadData();

            SubscribeOnEvent();

            SetUI();

            SetTask();
        }

        private void SetUI()
        {
            Sprite taskSprite = Resources.Load<Sprite>("TaskIcons/" + taskType.ToString());
            icon.sprite = taskSprite != null ? taskSprite : icon.sprite;
            info.text = LanguagesController.ActiveLanguagePreset.GetValue("task" + taskType.ToString() + "Info");
        }

        private void SetTask()
        {
            state = taskState.Find(x => x.status != TaskStatus.Complete);

            if (state != null)
            {
                task = taskData.Find(x => x.id == state.id);

                TaskState lastState = taskState.FindLast(x => x.status == TaskStatus.Complete);

                if (lastState != null)
                {
                    if (state.progress < lastState.progress)
                    {
                        state.progress = lastState.progress;
                        SetProgressTask(0);
                    }
                }

                switch (state.status)
                {
                    case TaskStatus.None:
                    case TaskStatus.InProgress:
                        {
                            state.status = TaskStatus.InProgress;
                            reward.color = defaultColor;
                        }
                        break;

                    case TaskStatus.WaitingReward:
                        {
                            SetProgressTask(0);
                        }
                        break;
                }
            }
            else
            {
                task = taskData[taskData.Count - 1];
                state = taskState[taskState.Count - 1];

                gameObject.SetActive(false);

                TaskComplete?.Invoke();

                UnsubscribeOnEvent();
            }

            statusLine.fillAmount = (float)state.progress / task.target;

            title.text = string.Format(LanguagesController.ActiveLanguagePreset.GetValue("task" + taskType.ToString()), LanguagesController.ActiveLanguagePreset.GetValue(task.target.ToShort()));
            reward.text = task.reward.ToShort();
            target.text = state.progress.ToShort() + " / " + task.target.ToShort();
        }

        private void SubscribeOnEvent()
        {
            switch (taskType)
            {
                case TaskType.None:
                    Debug.LogError("Task type None");
                    break;

                case TaskType.Likes:
                    PostStatistic.ChangedLikes = SetProgressTask;
                    break;

                case TaskType.Followers:
                    PostStatistic.ChangedFollowers = SetProgressTask;
                    break;

                case TaskType.Locations:
                    ChoosePanel.ChangedLocations = SetProgressTask;
                    break;

                case TaskType.FortuneWheel:
                    FortuneWheelController.ChangedFortuneWheel = SetProgressTask;
                    break;

                case TaskType.VideoRecords:
                    VideoRecorder.ChangedVideoRecords = SetProgressTask;
                    break;

                default:
                    Debug.LogError("Task type default");
                    break;
            }
        }

        private void UnsubscribeOnEvent()
        {
            PostStatistic.ChangedLikes = null;
            PostStatistic.ChangedFollowers = null;
            ChoosePanel.ChangedLocations = null;
            FortuneWheelController.ChangedFortuneWheel = null;
            VideoRecorder.ChangedVideoRecords = null;
        }

        public void SetProgressTask(long progress)
        {
            state.progress += progress;

            target.text = state.progress.ToShort() + " / " + task.target.ToShort();
            statusLine.fillAmount = (float)state.progress / task.target;

            if (state.progress >= task.target)
            {
                state.status = TaskStatus.WaitingReward;

                reward.color = rewardColor;

                collectButton.SetActive(true);

                waitingReward = true;

                TaskWaitingReward?.Invoke();
            }

            SaveData();
        }

        public void GetReward()
        {
            UICamera.SetCoinsEffectPosition(collectButton.transform);
            UICamera.PlayCoinsEffect();

            collectButton.SetActive(false);

            RuntimeDB.localUser.currency.Coins.Value += task.reward;

            state.status = TaskStatus.Complete;

            waitingReward = false;

            TaskWaitingReward?.Invoke();

            SetTask();

            SaveData();
        }

        #region Save Load Data

        private void SaveData()
        {
            PlayerPrefs.SetString(PP_KEY, JSON.To(taskState));
        }

        private void LoadData()
        {
            if (PlayerPrefs.HasKey(PP_KEY))
            {
                taskState = JSON.From<List<TaskState>>(PlayerPrefs.GetString(PP_KEY));
            }
            else
            {
                taskState = new List<TaskState>();

                taskData.ForEach(x =>
                {
                    taskState.Add(new TaskState() { id = x.id, status = TaskStatus.None, type = taskType });
                });

                SaveData();
            }
        }

        #endregion

        #region Debug

        [ContextMenu("Add One Thousand Progress")]
        public void AddOneThousandProgress()
        {
            SetProgressTask(1000);
        }

        [ContextMenu("Add Ten Thousand Progress")]
        public void AddTenThousandProgress()
        {
            SetProgressTask(10000);
        }

        #endregion
    }
}
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    [Serializable]
    public class Task : MonoBehaviour
    {
        public enum TaskType { None = -1, Likes, Followers, Locations, FortuneWheel, VideoRecords };
        
        public enum TaskStatus { None = -1, InProgress, WaitingReward, Complete };

        [Serializable]
        public class TaskState
        {
            public string id;
            public long progress;
            public TaskType type;
            public TaskStatus status;
        }

        [Serializable]
        public class TaskData
        {
            public string id;
            public long target;
            public long reward;
        }

        [Serializable]
        public class TaskInfo
        {
            public List<TaskData> Likes;
            public List<TaskData> Followers;
            public List<TaskData> Locations;
            public List<TaskData> FortuneWheel;
            public List<TaskData> VideoRecord;
        }
    }
}